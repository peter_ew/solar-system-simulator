from Tkinter import *
import time
import math
import os

balls=[] #Objects' x,y coordinates
ballspeed=[] #Objects' x,y speeds
ballr=[] #Objects' radiuses
ballcolour=[] #Objects' colours
freeball=False #Is there an object that is currently held by the mouse
line=[0,0,0,0] #The acceleration line
centerx=400 #X-Centre of the screen
centery=300 #Y-Centre of the screen
explosions=[] #Explosions' x,y coordinates and radiuses
newballr=1 #The radius of the next ball to be created

def mouseDown(event):
    global freeball,line,balls,newballr
    x=event.x
    y=event.y
    #Comet button clicked
    if event.x>=700 and event.x<=795 and event.y>=5 and event.y<=25:
        newballr=1
        return
    #Planet button clicked
    if event.x>=700 and event.x<=795 and event.y>=30 and event.y<=50:
        newballr=10
        return
    #Sun button clicked
    if event.x>=700 and event.x<=795 and event.y>=55 and event.y<=75:
        newballr=25
        return
    #Else put a new object on the screen and hold it
    freeball=True
    balls.append([x,y])
    ballspeed.append([0,0])
    ballr.append(newballr)
    if newballr==1:
        ballcolour.append(["gray","white"]) #Comet
    elif newballr==10:
        ballcolour.append(["blue","green"]) #Planet
    else:
        ballcolour.append(["yellow","red"]) #Sun
    line=[0,0,0,0]

def mouseUp(event):
    global freeball,ballspeed
    if freeball==False:
        return
    #Release the object currently being held by the mouse and accelerate it
    x=event.x
    y=event.y
    freeball=False
    ballspeed[-1]=[x-balls[-1][0],y-balls[-1][1]]
    

def mouseMotion(event):
    global line,freeball
    if freeball==False:
        return
    x=event.x
    y=event.y
    #Move the acceleration line
    line=[balls[-1][0],balls[-1][1],x,y]

def calculateAcceleration(temp,size):
    i=0
    while i<size:
        j=0
        while j<size:
            m2=ballr[i]**3 #Mass of the first object
            r=(balls[j][0]-balls[i][0])**2+(balls[j][1]-balls[i][1])**2 #Distance between the two objects
            if i==j:
                j=j+1
                continue
            if r==0:
                g=0
            else:
                g=m2/r
            #G=k*m1*m2/r^2
            #F=G=m2.a
            #a=G/m2=k*m1/r^2
            sin=(balls[i][1]-balls[j][1])/math.sqrt(r)
            cos=(balls[i][0]-balls[j][0])/math.sqrt(r)
            temp[j][0]=temp[j][0]+cos*g
            temp[j][1]=temp[j][1]+sin*g
            j=j+1
        i=i+1
    return (temp,size)

def applyAcceleration(temp,size):
    j=0
    while j<size:
        ballspeed[j][0]+=temp[j][0]
        ballspeed[j][1]+=temp[j][1]
        balls[j][0]=balls[j][0]+(ballspeed[j][0]/60.0)
        balls[j][1]=balls[j][1]+(ballspeed[j][1]/60.0)
        balls[j][0]=balls[j][0]-(balls[0][0]-centerx)
        balls[j][1]=balls[j][1]-(balls[0][1]-centery)
        if balls[j][0]<=0 or balls[j][1]<=0 or balls[j][0]>=800 or balls[j][1]>=600:
            balls.remove(balls[j])
            ballspeed.remove(ballspeed[j])
            ballr[j]=0
            ballr.remove(0)
            ballcolour[j]=0
            ballcolour.remove(0)
            j=j-1
            size=size-1
        j=j+1
    return size

def checkCollisions(size):
    i=0
    while i<size:
        j=0
        while j<size:
            if i==j:
                j=j+1
                continue
            if (balls[i][0]-balls[j][0])**2+(balls[i][1]-balls[j][1])**2<(ballr[i]+ballr[j])**2:
                #The exact place of the collision (I can't explain it in less than three lines)
                explosionX=balls[i][0]+(balls[j][0]-balls[i][0])*(float(ballr[i])/(ballr[i]+ballr[j]))
                explosionY=balls[i][1]+(balls[j][1]-balls[i][1])*(float(ballr[i])/(ballr[i]+ballr[j]))
                explosions.append([explosionX,explosionY,1])
                if ballr[i]>ballr[j]:
                    balls.remove(balls[j])
                    ballspeed.remove(ballspeed[j])
                    ballr[j]=0
                    ballr.remove(0)
                    ballcolour[j]=0
                    ballcolour.remove(0)
                    if i>j:
                        i=i-1
                    j=j-1
                    size=size-1
                else:
                    balls.remove(balls[i])
                    ballspeed.remove(ballspeed[i])
                    ballr[i]=0
                    ballr.remove(0)
                    ballcolour[i]=0
                    ballcolour.remove(0)
                    i=i-1
                    if j>i:
                        j=j-1
                    size=size-1
            j=j+1
        i=i+1
    return size

def draw(canvas):
    time0=time.clock() #Start of simulation
    size=len(balls)-freeball
    temp=[[0]*2 for t in range(len(balls))] #Temporary array for the gravity accelerations
    (temp,size)=calculateAcceleration(temp,size)
    size=applyAcceleration(temp,size)
    size=checkCollisions(size)
    time2=time.clock() #End of simulation, Start of render
    canvas.delete("all") #Delete all objects (No memory leaks)
    canvas.create_rectangle(0,0,800,600,fill="black") #Black background
    for i in range(0,len(balls)):
        ball=balls[i]
        canvas.create_oval(ball[0]-ballr[i],ball[1]-ballr[i],ball[0]+ballr[i],ball[1]+ballr[i],fill=ballcolour[i][0],outline=ballcolour[i][1]) #Render comets/planets/suns
    if freeball:
        canvas.create_line(line[0],line[1],line[2],line[3],fill="white")
    for explosion in explosions:
        if explosion[2]<=7:
            canvas.create_oval(explosion[0]-explosion[2],explosion[1]-explosion[2],explosion[0]+explosion[2],explosion[1]+explosion[2],fill="green") #Increasing explosion
        else:
            canvas.create_oval(explosion[0]-14+explosion[2],explosion[1]-14+explosion[2],explosion[0]+14-explosion[2],explosion[1]+14-explosion[2],fill="green") #Decreasing explosion
        explosion[2]=explosion[2]+0.5
        if explosion[2]>=14:
            explosions.remove(explosion) #Not longer needed
    time1=time.clock() #End of render
    if int((time1-time0)*1000)<16:
        fps=60
    else:
        fps=1000/int((time1-time0)*1000)
    canvas.create_text(5,5,anchor="nw",text=str(fps)+" FPS",fill="white",font="Tahoma")
    canvas.create_text(5,25,anchor="nw",text=str(len(balls))+" objects",fill="white",font="Tahoma")
    canvas.create_text(5,45,anchor="nw",text="Render Time: "+str(round((time1-time2)*1000,2))+"ms",fill="white",font="Tahoma")
    canvas.create_text(5,65,anchor="nw",text="Simulation Time: "+str(round((time2-time0)*1000,2))+"ms",fill="white",font="Tahoma")
    canvas.create_text(5,85,anchor="nw",text="Total Time: "+str(round((time1-time0)*1000,2))+"ms",fill="white",font="Tahoma")
    #Comet button
    if newballr==1:
        canvas.create_rectangle(700,5,795,25,outline="white",fill="darkgreen")
    else:
        canvas.create_rectangle(700,5,795,25,outline="white")
    canvas.create_text(747,15,text="Comet",fill="white")
    #Planet button
    if newballr==10:
        canvas.create_rectangle(700,30,795,50,outline="white",fill="darkgreen")
    else:
        canvas.create_rectangle(700,30,795,50,outline="white")
    canvas.create_text(747,40,text="Planet",fill="white")
    #Sun button
    if newballr==25:
        canvas.create_rectangle(700,55,795,75,outline="white",fill="darkgreen")
    else:
        canvas.create_rectangle(700,55,795,75,outline="white")
    canvas.create_text(747,65,text="Sun",fill="white")
    #FPS Limited to 60
    if int((time1-time0)*1000)>=15:
        canvas.after(1,draw,canvas)
    else:
        canvas.after(int(16-(time1-time0)*1000),draw,canvas)

root=Tk()
root.wm_title("Physics Simulation")
if os.name=="nt":
    root.iconbitmap(bitmap='physics.ico')
else:
    root.iconbitmap(bitmap='@physics.xbm')
canvas=Canvas(root,width=800,height=600)
canvas.bind("<ButtonPress-1>",mouseDown)
canvas.bind("<ButtonRelease-1>",mouseUp)
canvas.bind("<B1-Motion>",mouseMotion)
canvas.pack()

balls.append([400,300])
ballspeed.append([0,0])
ballr.append(25)
ballcolour.append(["yellow","red"])

draw(canvas)

root.mainloop()
